**Proyecto para probar las configuraciones y primeros pasos con Selenium y Java**

Primer proyecto que contiene el código base para poder probar la configuración básica de un proyecto java con Selenium

Visita por favor [mi blog](http://www.javamexico.org/blogs/alucard1) dentro de la comunidad de Java México

---

## Caracteristicas 

La realización del proyecto se ha generado con los siguientes elementos:

1. JDK 1.8.0_161
2. STS  3.9.6.RELEASE
3. Apache Maven 3.5.4
4. S.O Windows 7 *También se ha probado en MAC OS :)*
5. ChromeDriver 74.0.3729.6

---